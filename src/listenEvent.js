const rpc = require('ethrpc');
const Web3 = require('web3');
const config = require('./config');

const web3 = new Web3(new Web3.providers.HttpProvider(config.url));


const getListAddress = () => {
    return ["0xdd6f499ecc6e4d7db51e9c596fe5e6175a1f5348", "0xba20b201138a163c1573aebac1053da57e7d0df0", "0x9d4995e559ec3637d3250678d8b16fc517c34ef1", "0x4dc76bab45ae2f764e5f44b563acbab266c7a1e4", "0x9de6e23aa8e3494a333c782d89381c8ed45e6256", "0x78d3657cf918ea913ae9d27b6e48a27e3c4ca37a", "0xce4cb760c77ad3d0a4dbd04b826beff68990bcdc", "0x55d01d98719549bc5efdb7b6ddda590c6b9a3d25", "0x57fc0c742dee2ad4e05743c8843d197552724725", "0x29928eb310e42570e34d16f193ddd3090d707ea3"]
}

let connectionConfiguration = {
    httpAddresses: [`${config.url}`],
    wsAddresses: [],
    ipcAddresses: [],
    networkID: config.networkId,
    connectionTimeout: 900000,
    errorHandler: function (err) { console.log(err)},
};

let onBlock = (block) => {
    (async () => {
        // console.log('block', blocks);
        console.log('***** block number ETH *****:', parseInt(block.number, 16));
        if(block.transactions.length === 0) return;

        let addresses = await getListAddress();
        if(addresses.length === 0) return;

        let blocks = web3.eth.getBlock(block.hash, true);
        // console.log('block', blocks)
        if(blocks.transactions.length === 0) return;

        for(let i =0; i< blocks.transactions.length; i++){
            // console.log('123');
            let tx = blocks.transactions[i];
            if(tx.value === '0') continue;
            let value = web3.fromWei(tx.value.toNumber(), 'ether');
            // console.log('value', web3.fromWei(tx.value.toNumber(), 'ether'));
            for(let j = 0; j< addresses.length; j++){
                // console.log('address', tx.to && addresses[j] && addresses[j].toLowerCase() === tx.to.toLowerCase());
                if(tx.to && addresses[j] && addresses[j].toLowerCase() === tx.to.toLowerCase()){
                    console.log('-------');
                    console.log(tx.to);
                    console.log(value);
                    /***
                     * insert transaction vao db
                     *
                     *
                     *
                     *
                     *
                     *
                     *
                     *
                     */

                    break;
                }
            }
        };
    })().catch(err => {
        console.log('err listen block', err);
    })
};

rpc.connect(connectionConfiguration, function (err) {
    if (err) {
        console.error("Failed to connect to Ethereum node.");
    } else {
        console.log("Connected to Ethereum node!");
        let blockStream =rpc.getBlockStream();
        blockStream.subscribeToOnBlockAdded(onBlock);
    }
});
/**
 * rpc dung de listent event khi co deposit
 * @type {{rpc}}
 */
module.exports = {
    rpc
}
