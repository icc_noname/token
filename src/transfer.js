const Web3 = require('web3');
const Tx  = require('ethereumjs-tx');

const config = require('./config');

const web3 = () => {
    return new Web3(new Web3.providers.HttpProvider(config.url));
}

const constructNewTx = (fromAddress, toAddress, amount, gasLimit, gasPrice, data, chainId) => {
    let balance = web3().eth.getBalance(fromAddress) / (10 ** 18);
    let totalAmount = amount + gasPrice * gasLimit / (10 ** 18);
    if(balance  < totalAmount) throw  new Error('Ethereum address not enough balance');
    var newTxParams = {
        nonce: "0x" + web3().eth.getTransactionCount(fromAddress).toString("16"),
        gasPrice: "0x" + gasPrice.toString("16"),
        gasLimit: "0x" + gasLimit.toString("16"),
        to: toAddress,
        value: "0x" + Number(web3().toWei(amount, "ether")).toString(16),
        data: data,
        chainId: chainId
    }
    return newTxParams;
}

//sign raw transaction with private key
const signRawTx = (rawTx, privateKey) => {
    var constructedTx = new Tx(rawTx);
    constructedTx.sign(privateKey);
    var serializedTx = constructedTx.serialize();
    var data = '0x' + serializedTx.toString('hex');
    return data;

}

//submit transaction to blockchain
const submitTransaction = (signedTX) => {
    return new Promise(function(resolve, reject){
        web3().eth.sendRawTransaction(signedTX, function (err, txId) {
            if (err) reject(err);
            else resolve(txId);
        });
    });
};

const getInstance = () => {
    let token = web3().eth.contract(config.tokenAbi);
    const instance = token.at(config.tokenAddress);
    return instance;
};

const  getTokenBalance = (address) => {
    return getInstance().balanceOf.call(address).toNumber();
};

const transferToken = async (toAddress, amount, privateKey) => {
    let balance = getTokenBalance(config.admin);
    if (amount > balance) throw  new Error('Token not enough balance');
    let payload = getInstance().transfer.getData(toAddress, amount);
    let newTx = constructNewTx(config.admin , config.tokenAddress, 0, config.gasLimit, config.gasPrice, payload , config.networkId);
    let bufferPrivateKey = new Buffer(privateKey, 'hex');
    let signedTx = signRawTx(newTx, bufferPrivateKey);

    return await submitTransaction(signedTx);
};

module.exports = {
    getTokenBalance,
    transferToken
}
